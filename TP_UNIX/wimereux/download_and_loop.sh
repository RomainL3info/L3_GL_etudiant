#!/bin/bash

adresseImage=http://wimereux.biz/Webcam_1/Photo.jpg
PHOTO="Photo"
cpt=1

if [ $# != 2 ]
#test de l'existence ou non d'arguments dans la ligne de commande
then
	echo "problème: veuillez renseigner le lien http et le dossier de sauvegarde des images"
else
	#boucle de récupération des images
	while (($cpt<=$1)) #nb d'images à récupérer
	do			
			#récupération de l'image			
			wget -P $2 -nd  -p -A jpg $adresseImage
			#téléchargement de l'image en la renommant pour éviter d'écraser la précédente
			mv $2/${PHOTO}.jpg $2/${PHOTO}$cpt.jpg
			cpt=$(($cpt+1))			
			sleep 60			
	done
fi

#creation du gif animé
convert $2/Photo*.jpg $2/anime.gif